#!/usr/bin/python3

import http.client
import json
import os
import tempfile
import subprocess
import sys
import http.client, urllib.parse

# https://src.fedoraproject.org/api/0/#pull_requests-tab

def get_json(url):

    # https://docs.python.org/3/library/http.client.html
    conn = http.client.HTTPSConnection("src.fedoraproject.org")
    conn.request("GET", url)
    r = conn.getresponse()

    if r.status != 200:
        print("error: {}" % r.reason)
        return None

    r_data = r.read()
    return json.loads(r_data)

def get_pr_data(id):

    pr_data = get_json("/api/0/tests/selinux/pull-request/{}".format(id))
    # enable-tmt
    branch_from = pr_data['branch_from']
    # psss
    repo_username = pr_data['repo_from']['user']['name']
    # fork/psss/tests/selinux
    repo_url_path = pr_data['repo_from']['url_path']

    diff_data = get_json("/api/0/tests/selinux/pull-request/{}/diffstats".format(id))
    paths = []
    for diff_path in diff_data:
        path = diff_path.rsplit(sep="/", maxsplit=1)[0]
        if not path in paths:
            paths.append(path)
#    path = next(iter(diff_data)).rsplit(sep="/", maxsplit=1)[0]
    return (repo_username, branch_from, paths)

def create_pr(title, branch_from, repo_from_username, token):
    params = urllib.parse.urlencode({
        'title': title,
        'branch_to': 'master',
        'branch_from': branch_from,
        'repo_from': "libsepol",
        'repo_from_username': repo_from_username,
        'repo_from_namespace': 'rpms',
        'initial_comment': 'DO NOT MERGE'
    })
    headers = {
        "Content-type": "application/x-www-form-urlencoded",
        "Authorization": "token {}".format(token)
    }

    conn = http.client.HTTPSConnection("src.fedoraproject.org")
    conn.request("POST", "/api/0/rpms/libsepol/pull-request/new", params, headers)
    response = conn.getresponse()
    print(response.status, response.reason, file=sys.stderr)
    data = response.read()
    print(data, file=sys.stderr)
    return json.loads(data)

def get_tests_yml(repo_username, branch_from, paths):

    tests_yml = """---
# Tests that run in all contexts
- hosts: localhost
  roles:
  - role: standard-test-beakerlib
    tags:
    - classic
    - container
    repositories:
    - repo: "https://src.fedoraproject.org/forks/{}/tests/selinux.git"
      version: "{}"
      dest: "selinux"
    tests:
""".format(repo_username, branch_from)

    for path in paths:
        tests_yml += "    - selinux/{}\n".format(path)
    return tests_yml

pr_id = sys.argv[1]
(repo_username, branch_from, path) = get_pr_data(pr_id)
tests_yml = get_tests_yml(repo_username, branch_from, path)

tmpdir = tempfile.mkdtemp()
os.chdir(tmpdir)
print(tmpdir, file=sys.stderr)

subprocess.run("git clone ssh://plautrba@pkgs.fedoraproject.org/rpms/libsepol".split())
os.chdir("libsepol")

subprocess.run("git remote add plautrba ssh://plautrba@pkgs.fedoraproject.org/forks/plautrba/rpms/libsepol.git".split())

f = open("tests/tests.yml","w")
f.write(tests_yml)
f.close()

subprocess.run("git add tests/tests.yml".split())
commit_message = "Test https://src.fedoraproject.org/tests/selinux/pull-request/{} - DO NOT MERGE".format(pr_id)
commit_branch = "tests-selinux-PR-{}".format(pr_id)
subprocess.run(["git", "commit", "-m", commit_message])
subprocess.run("git push plautrba HEAD:{} --force".format(commit_branch).split())

token = os.getenv("TOKEN")
if token is None:
    print("token is required, run export TOKEN=... ")
    sys.exit(1)

print("commit_message='{}'\ncommit_branch='{}'\n".format(commit_message, commit_branch), file=sys.stderr)
new_pr = create_pr(commit_message, commit_branch, 'plautrba', token)

print("New PR - https://src.fedoraproject.org/rpms/libsepol/pull-request/{}".format(new_pr['id']))
print("Do not forget to remove {}".format(tmpdir))

