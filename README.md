# CI for tests

Use Fedora CI to test tests from https://src.fedoraproject.org/tests/selinux

## Usage

~~~
./create-pr.py 72      
/tmp/tmp3mokaetr
...
[master 62a4220b19f9] Test https://src.fedoraproject.org/tests/selinux/pull-request/72 - DO NOT MERGE
 1 file changed, 13 insertions(+), 2 deletions(-)
...
To ssh://pkgs.fedoraproject.org/forks/plautrba/rpms/libsepol.git
 + 0a7de7be9f8a...62a4220b19f9 HEAD -> tests-selinux-PR-72 (forced update)
commit_message='Test https://src.fedoraproject.org/tests/selinux/pull-request/72 - DO NOT MERGE'
commit_branch='tests-selinux-PR-72'
...
New PR - https://src.fedoraproject.org/rpms/libsepol/pull-request/8
Do not forget to remove /tmp/tmp3mokaetr
~~~